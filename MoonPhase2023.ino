/******************************************************************************/
/*                                                                            */
/*   Furgalladas Schalter und Sensoren                                        */
/*                                                                            */
/*   All rights reserved. Distribution or duplication without previous        */
/*   written agreement of the owner prohibited.                               */
/*                                                                            */
/******************************************************************************/

/** \file

   moonPhaseBT - MoonPhase lamp bluetooth


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> moonPhase2023.ino                 </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                        </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                   </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho            </td></tr>
   <tr> <td> Date:     </td> <td> 28-July-2018 01:32:56      </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> moonPhase2023 </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/* Bluetooth module HC-05
   RX is digital pin 7 of arduino (connect to TX of HC05)
   TX is digital pin 8 of arduino (connect to RX of HC05)
   Default baudRate: 9600. 
   No AT commands are transmitted, only used vÃ­a software UART
   Pairing code: 1234
*/

#include <Wire.h>
#include <SoftwareSerial.h>
#include "RTClib.h"
#include "btMoon.h"
#include "SerialCommand.h"
#include "scheduler.h"
RTC_DS1307 rtc;

//Pin definitios
#define PHASE1PIN 3
#define PHASE2PIN 5
#define PHASE3PIN 6
#define BT_RX 7
#define BT_TX 8
#define PHASE4PIN 9
#define PHASE5PIN 10
#define NEWMOONPIN 11
#define LIGHTSENSORPIN 3

//local auxiliar functions from SerialCommand
void lightSensorActivation();
void setMoon();
void lightSensorSensitivity();
void customMoonLength();
void moonMode();
void startMode();
void unrecognized();
void modifyMoonState();

//Bluetooth serial port
SoftwareSerial btSerial = SoftwareSerial(BT_RX, BT_TX);

//Serial commander
SerialCommand SCmd(btSerial);  // passing software serial object

//Declare scheduler
Scheduler scheduler;

btMoon moon(PHASE1PIN, PHASE2PIN, PHASE3PIN, PHASE4PIN, PHASE5PIN, NEWMOONPIN, LIGHTSENSORPIN);

void setup() {

  //Init bluetooth module port
  btSerial.begin(9600);
  //Init serial port
  Serial.begin(9600);

  if (!rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1)
      ;
  }

  if (!rtc.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }

  // Serial.println("**************MEMORY DUMP **********");
  // for (int address = 0; address < 100; address++) {
  //   if (address % 10 == 0) {
  //     Serial.println();
  //     Serial.print("Dirección ");
  //     Serial.print(address);
  //     Serial.print(": ");
  //   }
  //   byte value = EEPROM.read(address);
  //   if (value < 0x10) {
  //     Serial.print("0");
  //   }
  //   Serial.print(value, HEX);
  //   Serial.print(" ");
  //   delay(10);  // Pequeña pausa para evitar sobrecargar el puerto serie
  // }
  // Serial.println("\n**************MEMORY DUMP **********");

  SCmd.addCommand("st", startMode);
  SCmd.addCommand("mo", moonMode);
  SCmd.addCommand("le", customMoonLength);
  SCmd.addCommand("se", lightSensorSensitivity);
  SCmd.addCommand("sm", setMoon);
  SCmd.addCommand("ls", lightSensorActivation);
  SCmd.addCommand("ms", modifyMoonState);
  SCmd.addCommand("ti", sleepTimerValue);
  SCmd.addDefaultHandler(unrecognized);  // Handler for command that isn't matched  (says "What?")
}


void loop() {
  t_outputLedType a_LedValues;
  DateTime now;
  bool b_cycleEnded;
  switch (scheduler.GetTaskExecution()) {
    case e_1msTask:
      break;
    case e_10msTask:
      break;
    case e_100msTask:
      /* Analyze state machine */
      switch (moon.getMoonStateMachine()) {
        case e_startup:
          if (moon.getMoonMode() == e_moonModeCustomLength) {
            //Start mode is custom mode, so fetch from EEPROM previous length and configure moon
            moon.setCustomLengthTimeMark();
            moon.setNewMoonTarget();
            moon.setMoonStateMachine(e_fadingIn);
          } else {
            //Mode is naturalLength
            now = rtc.now();
            moon.calculateOutputLedArray( now.secondstime(), e_moonModeNaturalLength, &a_LedValues,&b_cycleEnded);
            moon.setTargetLedValues(a_LedValues);
            moon.setMoonStateMachine(e_fadingIn);
          }
          break;
        case e_fadingIn:
          //Perform the fade in to moon target
          if (moon.performFade() == false) {
            moon.setMoonStateMachine(e_printMoon);
          }
          break;
        case e_fadingOut:
          // Perform moon fade out to moon off
          if (moon.performFade() == false) {
            moon.setMoonStateMachine(e_moonOff);
          }
          break;
        case e_printMoon:
          // Print current moon depending on selected mode: natural or customLength
          if (moon.getMoonMode() == e_moonModeCustomLength) {
            moon.calculateOutputLedArray( (millis() / 10), e_moonModeCustomLength, &a_LedValues,&b_cycleEnded);
            moon.printMoon(a_LedValues);
            if (b_cycleEnded == true){
              moon.updateTargetMoonOff();
              moon.setMoonStateMachine(e_fadingOut);
            }
          } else {
            now = rtc.now();
            moon.calculateOutputLedArray( now.secondstime(), e_moonModeNaturalLength, &a_LedValues,&b_cycleEnded);
            moon.printMoon(a_LedValues);
          }
          /* Check for light sensor activation to turn off moon automatically */
          if (moon.checkLightSensorLowLight()||(moon.getMoonEnabled()==false)) {
            moon.updateTargetMoonOff();
            moon.setMoonStateMachine(e_fadingOut);
          }
          /* And move state machine to e_fadingOut */
          break;
        case e_moonOff:
          // Moon is off, check for light on conditions
          // if conditions fulfilled => set to fading in
          if (moon.getMoonEnabled() && (!moon.checkLightSensorLowLight())) {
            //Perform fade in to current mode
            if (moon.getMoonMode() == e_moonModeCustomLength) {
              //continue with custom mode
              moon.calculateOutputLedArray( (millis() / 10), e_moonModeCustomLength, &a_LedValues,&b_cycleEnded);
              moon.setTargetLedValues(a_LedValues);
              moon.setMoonStateMachine(e_fadingIn);
            } else {
              //Mode is naturalLength
              now = rtc.now();
              moon.calculateOutputLedArray( now.secondstime(), e_moonModeNaturalLength, &a_LedValues,&b_cycleEnded);
              moon.setTargetLedValues(a_LedValues);
              moon.setMoonStateMachine(e_fadingIn);
            }

          } else {
            //Off conditions fulfilled, do nothing
          }
          break;
        default:
          //Not expected
          break;
      }
      break;
    case e_50msTask:
      break;
    case e_300msTask:
      break;
    case e_500msTask:
      break;
    case e_1000msTask:
      //Process command buffer
      SCmd.readSerial();
      break;
    case e_5000msTask:
      break;
    case e_TaskCounter:
      //Serial.println("NoTask");
      break;
  }
}

//Moon state: to turn moon on/off: ms:xx; where xx= 0 for disabled; xx=1 for enabled
void modifyMoonState() {
  uint8_t option;
  char *arg;
  Serial.println("Rx: moonState");
  arg = SCmd.next();
  if (arg != NULL) {
    option = atol(arg);
    moon.modifyMoonState((bool)option);
  }
}

//start mode
void startMode()  //st:xx; //Preferred Start mode(0=> Natural, 1=> customLength)
{
  Serial.println("Rx: startMode");
  uint8_t option;
  char *arg;
  arg = SCmd.next();
  if (arg != NULL) {
    option = atol(arg);
    if (option == e_moonModeNaturalLength) {
      moon.setMoonMode(e_moonModeNaturalLength);
    } else {
      moon.setMoonMode(e_moonModeCustomLength);
    }
    moon.dumpConfigToEeprom();
  }
}

//Moon mode
void moonMode()  //mo:0;//Mode (0=> Natural, 1=> customLength)
{
  uint8_t option;
  char *arg;
  Serial.println("Rx: moonMode");
  arg = SCmd.next();
  if (arg != NULL) {
    option = atol(arg);
    if (option == e_moonModeNaturalLength) {
      moon.setMoonMode(e_moonModeNaturalLength);
    } else {
      moon.setMoonMode(e_moonModeCustomLength);
    }
    moon.dumpConfigToEeprom();
  }
}
//
void customMoonLength()  //Lenght of custom moon cycle (le:xx,yy;)
{
  int hours, minutes;
  char *arg;
  Serial.println("Rx: customLengtht");
  arg = SCmd.next();
  if (arg != NULL)  //convert hours
  {
    hours = atoi(arg);  // Converts a char string to an integer
  }

  arg = SCmd.next();
  if (arg != NULL) {
    minutes = atol(arg);
  }
  moon.setCustomLength(((uint32_t)hours * 3600 + (uint32_t)minutes * 60) * 100);  //Set custom length in seconds*100
  moon.dumpConfigToEeprom();
  moon.turnOffMoon();
  moon.setCustomLengthTimeMark();
  moon.setMoonMode(e_moonModeCustomLength);
  moon.setNewMoonTarget();
  moon.setMoonStateMachine(e_printMoon);
}

// Sleep timer confituration
void sleepTimerValue()  //Lenght of custom moon cycle (ti:xx,yy;)
{
  int hours, minutes;
  char *arg;
  Serial.println("Rx: customLengtht");
  arg = SCmd.next();
  if (arg != NULL)  //convert hours
  {
    hours = atoi(arg);  // Converts a char string to an integer
  }

  arg = SCmd.next();
  if (arg != NULL) {
    minutes = atol(arg);
  }
  moon.setSleepTimer(((uint32_t)hours * 3600 + (uint32_t)minutes * 60));  //Set custom length in seconds
  moon.dumpConfigToEeprom();
}

//Light sensor sensitivity: se:0;
void lightSensorSensitivity() {
  char *arg;
  arg = SCmd.next();
  Serial.println("Rx: lightSensorSensitivity");
  if (arg != NULL) {
    moon.setLightSensorSensitivity(atol(arg));
    moon.dumpConfigToEeprom();
  }
}

//Set moon command: sm:xyz;
void setMoon() {
  byte secondsConverted, minutesConverted, hoursConverted, daysConverted;
  DateTime now;
  t_outputLedType a_LedValues;
  char *arg;
  arg = SCmd.next();
  Serial.println("Rx: setMoon");
  if (arg != NULL) {
    moon.percentToDate(atol(arg), &daysConverted, &hoursConverted, &minutesConverted, &secondsConverted);
    rtc.adjust(DateTime(2000, 1, daysConverted + 1, hoursConverted, minutesConverted, secondsConverted));
    moon.turnOffMoon();
    moon.setMoonMode(e_moonModeNaturalLength);
  }
}

//Light sensor activation command: ls:0;
void lightSensorActivation() {
  uint8_t option;
  char *arg;
  Serial.println("Rx: lightSensorActivation");
  arg = SCmd.next();
  if (arg != NULL) {
    option = atol(arg);
    if (option == 0) {
      if (moon.getLightSensorActivation()) {  //Change Status: update and store in EEPROM
        moon.setLightSensorActivation(false);
      }
    } else {
      if (!moon.getLightSensorActivation()) {  //Change Status: update and store in EEPROM
        moon.setLightSensorActivation(true);
      }
    }
    moon.dumpConfigToEeprom();
  }
}

// This gets set as the default handler, and gets called when no other command matches.
void unrecognized() {
  Serial.println("What?");
}
