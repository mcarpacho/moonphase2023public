typedef uint8_t byte;
typedef uint32_t timeStampType;

//State machine
typedef enum 
{
  e_startup, 
  e_fadingIn,
  e_fadingOut,
  e_printMoon,
  e_moonOff
} t_e_moonStateMachine; 

//fading action
typedef enum
{
  e_noFade  = 0,
  e_fadeIn  = 1,
  e_fadeOut = 2
}t_e_fadingType;

typedef enum
{
  e_moonModeNaturalLength,
  e_moonModeCustomLength
}t_e_moonMode;

typedef struct
{
  unsigned long customLengthModeStartTimeMark; //Start time mark (millis())
  unsigned long customLengthModeFullCycleCentSeconds; //number of seconds in a full cycle (the input length basically)
  unsigned long customLengthModeFullMoonCentSeconds; // number of seconds from new moon to full moon (so customLengthModeFullCycleSeconds / 2)
  unsigned long customLengthModeColumnCentSeconds; //Seconds of a column: customLengthModeFullMoonSeconds / 5
  unsigned long customLengthModeLedLevelCentSeconds; //Seconds for every led in one column (minimum light resolution of the lamp, depending on the length of chosen cie
  unsigned long customLengthModeSleepTimer;          //Number of seconds after that moon will turn off after performing the full cycle
}t_st_customLengthModeConfig;

typedef struct
{
  uint8_t u08_currentLedStateIdx[6]; //current led state index in CIE256 (0 to 5 => moon led columns; 6=>newMoon led)
  uint8_t u08_targetLedStateIdx[6]; //Target led state, to perform fadeIn/FadeOut
  t_e_fadingType e_fading;          //fading action 
  bool b_moonEnabled; 
} t_st_moonState;


typedef struct
{
  uint32_t u32_magicFlag;
  bool b_lightSensorActivation; 
  uint16_t u16_lightSensorSensitivity; 
  uint32_t u32_customLength; 
  uint32_t u32_sleepTimer;
  t_e_moonMode e_startMoonMode; 
}t_st_moonEepromConfig; 

typedef uint8_t u08_outputLedValues[6];
typedef u08_outputLedValues t_outputLedType;

