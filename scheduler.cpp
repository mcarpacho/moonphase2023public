/** \file

   scheduler.cpp - implementation file for scheduler class


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> scheduler.cpp                    </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                              </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                         </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho                  </td></tr>
   <tr> <td> Date:     </td> <td> 15-August-2022 22:25:56          </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> basicScheduler </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/*MIT License

Copyright (c) [2022] [M.Carpacho]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#ifndef _SCHEDULER_CPP_
#define _SCHEDULER_CPP_

#include "scheduler.h"


Scheduler::Scheduler(void){
  unsigned long int currentTime;
  //Initialize deadline lists and timeouts
  currentTime = millis();
  for (uint8_t iterator =0;iterator<e_TaskCounter; iterator++){
    schedulerTaskList[iterator].deadline = currentTime + schedulerTaskList[iterator].taskDuration; 
  }
}

t_e_schedulerTasks Scheduler::GetTaskExecution(void){
  unsigned long int currentTime;
  uint8_t taskReturnValue = NO_TASK;

  currentTime = millis();
  for(uint8_t taskCounter=e_TaskCounter-1; taskCounter!=UINT8_MAX; taskCounter--){
    if (schedulerTaskList[taskCounter].deadline<currentTime)
    {
      taskReturnValue=taskCounter;
      /* Update new deadline */
      schedulerTaskList[taskCounter].deadline = currentTime + schedulerTaskList[taskCounter].taskDuration; 
      break; 
    }
  }
  
  return taskReturnValue;
}


#endif // _SCHEDULER_CPP_
