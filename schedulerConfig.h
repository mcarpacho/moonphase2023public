/** \file

   schedulerConfig.h - Configuration file for scheduler


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> schedulerConfig.h                </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                              </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                         </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho                  </td></tr>
   <tr> <td> Date:     </td> <td> 15-August-2022 22:25:56          </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> basicScheduler </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/*MIT License

Copyright (c) [2022] [M.Carpacho]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef _SCHEDULERCONFIG_H_
#define _SCHEDULERCONFIG_H_

//Configure deadlines
#define _1MS_TASK_VALUE      1
#define _10MS_TASK_VALUE     10
#define _50MS_TASK_VALUE     50
#define _100MS_TASK_VALUE    100
#define _300MS_TASK_VALUE    300
#define _500MS_TASK_VALUE    500
#define _1000MS_TASK_VALUE   1000
#define _5000MS_TASK_VALUE   5000
#define NO_TASK              0

//Scheduler tasks
typedef enum 
{
  e_1msTask    = 0u, 
  e_10msTask   = 1u,
  e_50msTask   = 2u,
  e_100msTask  = 3u,
  e_300msTask  = 4u,
  e_500msTask  = 5u,
  e_1000msTask = 6u,
  e_5000msTask = 7u,
  e_TaskCounter= 8u
} t_e_schedulerTasks;

typedef struct
{
  unsigned long deadline; 
  unsigned int taskDuration;
} t_st_schedulerListEntry; 



#endif /* _SCHEDULERCONFIG_H_ */
