#include "Arduino.h"

#include "btMoon.h"
#include <EEPROM.h>

//Constructor
btMoon::btMoon(byte InputRow1Pin, byte InputRow2Pin, byte InputRow3Pin, byte InputRow4Pin, byte InputRow5Pin, byte InputNewMoonPin, byte lightSensor){
	pinMode (InputRow1Pin, OUTPUT);
	pinMode (InputRow2Pin, OUTPUT);
	pinMode (InputRow3Pin, OUTPUT);
	pinMode (InputRow4Pin, OUTPUT);
	pinMode (InputRow5Pin, OUTPUT);
	pinMode (InputNewMoonPin, OUTPUT);
	inputRowPinArray[0] = InputRow1Pin;
	inputRowPinArray[1] = InputRow2Pin;
	inputRowPinArray[2] = InputRow3Pin;
	inputRowPinArray[3] = InputRow4Pin;
	inputRowPinArray[4] = InputRow5Pin;
	inputRowPinArray[5] = InputNewMoonPin;
	lightSensorPin = lightSensor;
  //Fetch data from EEPROM
  //Detect if EEPROM is uninitialized
  EEPROM.get(EEPROM_BASE_ADDRESS, st_moonEepromConfig);
  if (st_moonEepromConfig.u32_magicFlag != EEPROM_MAGIC_FLAG){
    Serial.println("eep uninit");
    //EEPROM uninitialized. Load default
    st_moonEepromConfig.u32_magicFlag = EEPROM_MAGIC_FLAG;
    st_moonEepromConfig.b_lightSensorActivation = false;
    st_moonEepromConfig.u16_lightSensorSensitivity = 0xFA;
    st_moonEepromConfig.u32_customLength = 360000;//Default custom: 1h 
    st_moonEepromConfig.e_startMoonMode = e_moonModeCustomLength;
    st_moonEepromConfig.u32_sleepTimer = SLEEP_TIMER_DEFAULT_VALUE;
    EEPROM.put(EEPROM_BASE_ADDRESS, st_moonEepromConfig);
  }

  //Activate moon: 
  st_moonState.b_moonEnabled = true; 

  //Set state machine
  this->e_moonStateMachine = e_startup;

  //Copy previous customLength to mirror
  st_customLengthModeConfig.customLengthModeFullCycleCentSeconds = st_moonEepromConfig.u32_customLength;
  //Calculate values
  st_customLengthModeConfig.customLengthModeFullMoonCentSeconds = st_customLengthModeConfig.customLengthModeFullCycleCentSeconds / 2; 
  st_customLengthModeConfig.customLengthModeColumnCentSeconds = st_customLengthModeConfig.customLengthModeFullMoonCentSeconds / 5; 
  st_customLengthModeConfig.customLengthModeLedLevelCentSeconds = st_customLengthModeConfig.customLengthModeColumnCentSeconds / 256;
  st_customLengthModeConfig.customLengthModeSleepTimer = st_moonEepromConfig.u32_sleepTimer;
}


void btMoon::HandleLed(byte ledRow, byte value){	
	analogWrite(ledRow,value);
}

void btMoon::modifyMoonState(bool inputMoonState){
  st_moonState.b_moonEnabled = inputMoonState;
}

//New version

void btMoon::dumpConfigToEeprom(){
  EEPROM.put(EEPROM_BASE_ADDRESS, st_moonEepromConfig);
}

bool btMoon::getLightSensorActivation(){
  return st_moonEepromConfig.b_lightSensorActivation;  
}
void btMoon::setLightSensorActivation(bool lightSensorStatus){
  st_moonEepromConfig.b_lightSensorActivation = lightSensorStatus;
}

void btMoon::setLightSensorSensitivity(uint16_t sensitivity){
  //Input sensitivity goes from 0 to 100 => convert to something in range of photoresistor
  st_moonEepromConfig.u16_lightSensorSensitivity=(uint16_t)((800*(uint32_t)sensitivity)/100);
  //Save to EEPROM
  EEPROM.put(EEPROM_BASE_ADDRESS, st_moonEepromConfig);
}

t_e_moonMode btMoon::getMoonMode(){
  return st_moonEepromConfig.e_startMoonMode;
}

void btMoon::setMoonMode(t_e_moonMode inputMode){
  st_moonEepromConfig.e_startMoonMode=inputMode;
}

boolean btMoon::getMoonEnabled(){
  return st_moonState.b_moonEnabled;
}


void btMoon::setNewMoonTarget(){ //Set new moon leds and config
  for (uint8_t u08_index=0; u08_index<5u; u08_index++){
    HandleLed(u08_index, pgm_read_byte_near(cie256+0u));
    st_moonState.u08_currentLedStateIdx[u08_index]=0;
    st_moonState.u08_targetLedStateIdx[u08_index]=0; 
  }
  st_moonState.u08_currentLedStateIdx[5]=0;
  st_moonState.u08_targetLedStateIdx[5]=255;//only new moon row activated
  //set fading in
  st_moonState.e_fading = e_fadeIn;
}



boolean btMoon::performFade(){ //Set new moon leds and config
  bool b_return = false;
  //update moon leds 
  for (uint8_t u08_idx=0; u08_idx<6; u08_idx++)
  {
    if (st_moonState.u08_currentLedStateIdx[u08_idx] != st_moonState.u08_targetLedStateIdx[u08_idx])
    {
      //Update values
      if (st_moonState.e_fading==e_fadeIn)
      {
        //Serial.println("fading in");
        if (((uint16_t)st_moonState.u08_currentLedStateIdx[u08_idx]+FADEIN_DECIMATE_FACTOR)>st_moonState.u08_targetLedStateIdx[u08_idx])
        {
          st_moonState.u08_currentLedStateIdx[u08_idx] = st_moonState.u08_targetLedStateIdx[u08_idx];
        }else{
          st_moonState.u08_currentLedStateIdx[u08_idx]=st_moonState.u08_currentLedStateIdx[u08_idx]+FADEIN_DECIMATE_FACTOR;
        }
        
      }else{
        if (st_moonState.e_fading==e_fadeOut){
          //Serial.println("fading out");
          if (st_moonState.u08_currentLedStateIdx[u08_idx]<FADEOUT_DECIMATE_FACTOR)
          {
            st_moonState.u08_currentLedStateIdx[u08_idx] = 0;
          }else{
            st_moonState.u08_currentLedStateIdx[u08_idx]=st_moonState.u08_currentLedStateIdx[u08_idx]-FADEOUT_DECIMATE_FACTOR;
          }
        }else{
          //No fade case
          //Serial.println("warn, no fade case");
        }
      }
      //Read from progmem with pgm_read_byte_near funcion, check https://www.arduino.cc/reference/en/language/variables/utilities/progmem/
      HandleLed(inputRowPinArray[u08_idx], pgm_read_byte_near(cie256+st_moonState.u08_currentLedStateIdx[u08_idx]));
      b_return = true;
    }
  }

  if (b_return == false)
  {
    st_moonState.e_fading = e_noFade;
  }

  // Serial.print("Fading, origin: ");
  // for (uint8_t idx = 0; idx<6; idx++){
  //   Serial.print(st_moonState.u08_currentLedStateIdx[idx]); 
  //   Serial.print(", ");
  // }
  // Serial.println();
  // Serial.print("Fading, target: ");
  // for (uint8_t idx = 0; idx<6; idx++){
  //   Serial.print(st_moonState.u08_targetLedStateIdx[idx]); 
  //   Serial.print(", ");
  // }
  // Serial.println();
  return b_return; 
}

void btMoon::setCustomLength(uint32_t inCustomLength){
  //Store also in EEPROM 
  st_moonEepromConfig.u32_customLength = inCustomLength;
  st_customLengthModeConfig.customLengthModeFullCycleCentSeconds = inCustomLength;
  st_customLengthModeConfig.customLengthModeFullMoonCentSeconds = st_customLengthModeConfig.customLengthModeFullCycleCentSeconds / 2; 
  st_customLengthModeConfig.customLengthModeColumnCentSeconds = st_customLengthModeConfig.customLengthModeFullMoonCentSeconds / 5; 
  st_customLengthModeConfig.customLengthModeLedLevelCentSeconds = st_customLengthModeConfig.customLengthModeColumnCentSeconds / 256;
}

void btMoon::setSleepTimer(uint32_t inSleepTimer){
  //Store also in EEPROM 
  st_moonEepromConfig.u32_sleepTimer = inSleepTimer;
}

void btMoon::percentToDate(uint8_t percentaje, byte *daysConverted, byte *hoursConverted, byte *minutesConverted, byte *secondsConverted){	
	uint16_t auxSeconds;
  uint32_t percentSeconds;
  percentSeconds=(MOON_COMPLETE_CYCLE* percentaje)/100; //Convert percent to seconds elapsed 
  //Now convert percentSeconds to a day, hours, min, seconds
	*daysConverted =(byte)(percentSeconds/SECONDS_IN_A_DAY);
	auxSeconds = percentSeconds%SECONDS_IN_A_DAY;
	*hoursConverted = auxSeconds/SECONDS_IN_A_HOUR;
	auxSeconds = auxSeconds%SECONDS_IN_A_HOUR;
	*minutesConverted = auxSeconds/SECONDS_IN_A_MINUTE;
	*secondsConverted = auxSeconds%SECONDS_IN_A_MINUTE;
}

void btMoon::setCustomLengthTimeMark(){
  st_customLengthModeConfig.customLengthModeStartTimeMark = millis()/10;
}

void btMoon::updateTargetMoonOff(){
  for (uint8_t i=0; i<6; i++){
    st_moonState.u08_targetLedStateIdx[i] = 0;
  }
  st_moonState.e_fading = e_fadeOut;
}

void btMoon::setTargetLedValues(t_outputLedType a_LedValues){
  for (uint8_t u08_idx=0; u08_idx<6; u08_idx++){
    st_moonState.u08_targetLedStateIdx[u08_idx]=a_LedValues[u08_idx];
  }
  st_moonState.e_fading = e_fadeIn;
}

boolean btMoon::checkLightSensorLowLight(){
  if ((uint16_t)(analogRead(lightSensorPin)>st_moonEepromConfig.u16_lightSensorSensitivity)&&(st_moonEepromConfig.b_lightSensorActivation==true)){
    return true;
  }
  return false; 
}


t_e_moonStateMachine btMoon::getMoonStateMachine(){
  return this->e_moonStateMachine;
}


void btMoon::setMoonStateMachine(t_e_moonStateMachine inputStateMachineState){
  this->e_moonStateMachine = inputStateMachineState;
}

//Function that turn off moon without fading
void btMoon::turnOffMoon(){
  for (uint8_t idx=0; idx<6; idx++){ //Apago todo
    HandleLed (inputRowPinArray[idx], pgm_read_byte_near(cie256+0x00));//LEDS COMPLETAMENTE APAGADOS
    st_moonState.u08_currentLedStateIdx[idx]=0u;
  }
}


//Function that will handle the input led array and set the current status of the leds
void btMoon::printMoon(t_outputLedType a_LedValues){
  for (uint8_t u08_idx =0; u08_idx<6; u08_idx++){
    HandleLed(inputRowPinArray[u08_idx], pgm_read_byte_near(cie256+a_LedValues[u08_idx]));
    //Copy current status to track status to perform fades
    st_moonState.u08_currentLedStateIdx[u08_idx]=a_LedValues[u08_idx];
  }
}


void btMoon::calculateOutputLedArray(uint32_t u32_timeMark,t_e_moonMode e_inputMode, t_outputLedType* a_outputLedValues, bool* b_cycleEnded){
	uint32_t currentSecondInCycle = 0; 
	uint32_t activeRow, modactiveRow=0, newMoonSubPhase;
  uint16_t subPhase;
  uint32_t u32_moonCompleteCycle, u32_moonHalfCycle, u32_secondsInARow, u32_subphaseNumberSeconds,u32_newMoonSubphaseSec; 

  //Init variable cycle ended
  *b_cycleEnded = false;

  //First, reset vector
  for (uint8_t u08_iterator = 0; u08_iterator < 6; u08_iterator++){
    (*a_outputLedValues)[u08_iterator]= 0u; 
  }

  if(e_inputMode == e_moonModeNaturalLength){
    u32_moonCompleteCycle = MOON_COMPLETE_CYCLE;
    u32_moonHalfCycle = MOON_HALF_CYCLE;
    u32_secondsInARow = SECONDS_IN_A_ROW;
    u32_subphaseNumberSeconds = SUBPHASE_NUMBER_SECONDS; 
    u32_newMoonSubphaseSec = NEWMOON_SUBPHASE_SEC; 
    //Calculate currentSecondInCycle
    currentSecondInCycle = u32_timeMark % u32_moonCompleteCycle;
  }else{
    u32_moonCompleteCycle = st_customLengthModeConfig.customLengthModeFullCycleCentSeconds;
    u32_moonHalfCycle = st_customLengthModeConfig.customLengthModeFullMoonCentSeconds;
    u32_secondsInARow = st_customLengthModeConfig.customLengthModeColumnCentSeconds;
    u32_subphaseNumberSeconds = st_customLengthModeConfig.customLengthModeLedLevelCentSeconds; 
    u32_newMoonSubphaseSec = st_customLengthModeConfig.customLengthModeFullMoonCentSeconds/255; 
    //Calculate currentSecondInCycle
    currentSecondInCycle = u32_timeMark - st_customLengthModeConfig.customLengthModeStartTimeMark;
    if (currentSecondInCycle>st_customLengthModeConfig.customLengthModeFullCycleCentSeconds){
      //Stop performing when one cycle has been completed
      //Detection of sleep time after finished cycle
      if ((currentSecondInCycle-st_customLengthModeConfig.customLengthModeFullCycleCentSeconds)>st_customLengthModeConfig.customLengthModeSleepTimer){
        //Sleep timer ended => Inform that cycle has ended
        *b_cycleEnded = true;
        //turn off moon
        st_moonState.b_moonEnabled = false; 
      }
      currentSecondInCycle = st_customLengthModeConfig.customLengthModeFullCycleCentSeconds;
    }
  }
  
	if (currentSecondInCycle < u32_moonHalfCycle){//Moon status: crescent
		
		activeRow = floor(currentSecondInCycle / u32_secondsInARow); //Day in phase represent the current row
		modactiveRow = currentSecondInCycle % u32_secondsInARow;		
		subPhase = modactiveRow / u32_subphaseNumberSeconds;
          
    if (subPhase>255){
      subPhase = 255; //Limit subphase to 255. This can happen due to roundings in calculations (detected when calculating u32_subphaseNumberSeconds)
      //It is not critical to loose some cycles at the end of one row, so with this, we will avoid using float calculations. 
    } 
		//New moon calculation
		newMoonSubPhase = floor (currentSecondInCycle / (u32_newMoonSubphaseSec));
    if (newMoonSubPhase>255){
      //Avoid overflow due to possible rounding problems
      newMoonSubPhase = 255;
    }
    
    //Columns that are fully on
		for (uint8_t i=0; i<activeRow; i++){
      (*a_outputLedValues)[i] = 255u;
      //Save current status 
		}
    //Column with no entire light
    (*a_outputLedValues)[activeRow] = subPhase;
		
    if (newMoonSubPhase>255) newMoonSubPhase = 255; //avoid overflow due to rounding
    //Finally, set the new moon column
    (*a_outputLedValues)[NEW_MOON_ROW_NUMBER] = 255-newMoonSubPhase;
		
	}else{//Moon status: decreasing
		activeRow = ceil((currentSecondInCycle-u32_moonHalfCycle) / u32_secondsInARow);
		modactiveRow = currentSecondInCycle % u32_secondsInARow;		
		subPhase = floor(modactiveRow / u32_subphaseNumberSeconds);
    if (subPhase>255){
      subPhase = 255; //Avoid frontier case
    } 
		//New moon calculation
		newMoonSubPhase = floor ((currentSecondInCycle-u32_moonHalfCycle) / (u32_moonHalfCycle/256));
    if (newMoonSubPhase>255){
      //Avoid overflow due to possible rounding problems
      newMoonSubPhase = 255;
    }
		//Process now output vector: clear first dark columns
    for (uint8_t i=0; i<activeRow;i++){
      (*a_outputLedValues)[i] = 0u;
    }
    //Now, process active column
		for (uint8_t i=activeRow+1; i<5; i++){
      (*a_outputLedValues)[i] = 255u;
		}
    //Finally, set remaining columns to fully light
    if (currentSecondInCycle % u32_secondsInARow == 0){
      (*a_outputLedValues)[activeRow] = 0;
    }else{
      (*a_outputLedValues)[activeRow] = 255-subPhase;
    };
    //And output the newMoon column
    (*a_outputLedValues)[NEW_MOON_ROW_NUMBER] = newMoonSubPhase;
	}
}

