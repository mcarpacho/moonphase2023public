#ifndef _BTMOON_H_
#define _BTMOON_H_


#include "btMoonTypes.h"
#include <inttypes.h>
#include <avr/pgmspace.h>
#include "cie1931_256.h"
// #include "cie1931_128.h"
// #include "cie1931_64.h"

//All defines here
#include <Arduino.h>

/* Moon designed with 5 rows of leds */
#define MOON_COMPLETE_CYCLE 	    2551443   /* Number of seconds in a full moon cycle of 29 days */
#define MOON_HALF_CYCLE 		      1275721   /* Half of Number of seconds in a full moon cycle of 29 days => MOON_COMPLETE_CYCLE / 2*/
#define SECONDS_IN_A_ROW          255144    /* Number of seconds in a row (remember, there are 5 rows of leds) =>  MOON_HALF_CYCLE / 5 */
#define SUBPHASE_NUMBER_SECONDS   996       /* Every row of leds (or column) has 256 possible values for the LED (See cie1931_256). Every position is equal to SECONDS_IN_A_ROW / 256 */
#define NEWMOON_SUBPHASE_SEC      4983      /* Number of seconds for every step of new moon; equivalent to MOON_HALF_CYCLE / 256 */
#define SECONDS_IN_A_DAY		      86400     /* Number of seconds in a day */
#define SECONDS_IN_A_HOUR		      3600      /* Number of seconds in an hour */
#define SECONDS_IN_A_MINUTE		    60        /* Number of seconds in a minute */
#define NEW_MOON_ROW_NUMBER       5u        /* Row number of new moon */
#define SLEEP_TIMER_DEFAULT_VALUE 3600u     /* Default value for sleep timer: 60 minutes */

#define SENSOR_THHRESHOLD		30

#define FADEIN_DECIMATE_FACTOR        10u
#define FADEOUT_DECIMATE_FACTOR       10u

//EEPROM addresses and config
#define EEPROM_BASE_ADDRESS             0x00
#define EEPROM_MAGIC_FLAG               0x83C045A1



class btMoon{
    
public:
	//Constructor
	btMoon(byte InputRow1Pin, byte InputRow2Pin, byte InputRow3Pin, byte InputRow4Pin, byte InputRow5Pin, byte InputNewMoonPin, byte lightSensor);
  void percentToDate(uint8_t percentaje, byte *daysConverted, byte *hoursConverted, byte *minutesConverted, byte *secondsConverted);
  bool getLightSensorActivation();
  void setLightSensorActivation(bool lightSensorStatus);
  void setLightSensorSensitivity(uint16_t sensitivity);
  void setCustomLength(uint32_t inCustomLength);
  void setSleepTimer(uint32_t inSleepTimer);
  void setCustomLengthTimeMark();
  void turnOffMoon();
  void setMoonMode(t_e_moonMode inputMode);
  t_e_moonStateMachine getMoonStateMachine();
  void setMoonStateMachine(t_e_moonStateMachine inputStateMachineState);  
  void setNewMoonTarget(); 
  boolean performFade();
  void dumpConfigToEeprom();
  void modifyMoonState(bool inputMoonState);
  boolean getMoonEnabled();
  t_e_moonMode getMoonMode();
  bool checkLightSensorLowLight();
  void updateTargetMoonOff(); 
  void calculateOutputLedArray(uint32_t u32_timeMark,t_e_moonMode e_inputMode, t_outputLedType* a_outputLedValues,bool* b_cycleEnded);
  void printMoon(t_outputLedType a_LedValues);
  void setTargetLedValues(t_outputLedType a_LedValues);
  
private:
	byte lightSensorPin;
  t_e_moonMode e_moonMode; 
	void HandleLed(byte ledRow, byte value);
	byte inputRowPinArray[6];
	// Bright led array
  t_st_moonState st_moonState;
  t_e_moonStateMachine e_moonStateMachine; // State machine
  t_st_customLengthModeConfig st_customLengthModeConfig;
  t_st_moonEepromConfig st_moonEepromConfig; 
};



#endif // _BTMOON_H_
