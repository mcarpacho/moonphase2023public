/** \file

   scheduler.h - header declaration file for scheduler


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> scheduler.h                      </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                              </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                         </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho                  </td></tr>
   <tr> <td> Date:     </td> <td> 15-August-2022 22:25:56          </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> basicScheduler </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/*MIT License

Copyright (c) [2022] [M.Carpacho]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef _SCHEDULER_H_
#define _SCHEDULER_H_

#include "Arduino.h"
#include "schedulerConfig.h"

//Class definitions
class Scheduler {
  public:
    //Constructor con lista de inicializadores de constructor
    Scheduler(void);
    t_e_schedulerTasks GetTaskExecution(void);
    
    
  private:
    //Internal variables
    t_st_schedulerListEntry schedulerTaskList[e_TaskCounter]={
      {0, _1MS_TASK_VALUE}, 
      {0, _10MS_TASK_VALUE},
      {0, _50MS_TASK_VALUE},
      {0, _100MS_TASK_VALUE},
      {0, _300MS_TASK_VALUE},
      {0, _500MS_TASK_VALUE}, 
      {0, _1000MS_TASK_VALUE},
      {0, _5000MS_TASK_VALUE}
    };
};


#endif // _SCHEDULER_H_
